package com.example.example;

import static android.app.job.JobInfo.PRIORITY_DEFAULT;
import static android.app.job.JobInfo.PRIORITY_HIGH;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.example.example.model.Notes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

public class UserNoteActivity extends AppCompatActivity {
    private TextView messageEditText;
    private EditText user_year;
    private EditText user_month;
    private EditText user_day;
    private Button save;
    Calendar today = Calendar.getInstance();
    private NotificationManager notificationManager;
    private static final int NOTIFY_ID = 1;
    private static final String CHANNEL_ID = "CHANNEL_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_note);

        messageEditText = findViewById(R.id.user_note);
        user_year = findViewById(R.id.user_year);
        user_month = findViewById(R.id.user_month);
        user_day = findViewById(R.id.user_day);
        save = findViewById(R.id.save);
        notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        save.setOnClickListener(v -> {
            String umessage = messageEditText.getText().toString();

            long umonth = 0;
            long uday = 0;
            long uyear = 0;

            if (Objects.equals(umessage, "") || user_year.getText() == null || Objects.equals(user_month.getText(), null) || Objects.equals(user_day.getText(), null)) {
                Toast.makeText(this, "Вы не ввели все поля", Toast.LENGTH_SHORT).show();
            }else {

                uyear = Integer.parseInt(user_year.getText().toString());
                umonth = Integer.parseInt(user_month.getText().toString());
                uday = Integer.parseInt(user_day.getText().toString());

                if (umonth >= 13 || uday >= 32) {
                    Toast.makeText(this, "Неправильная дата", Toast.LENGTH_SHORT).show();
                } else {
                        MainActivity.appDatabase.noteDao().insert(new Notes(umessage, uyear, umonth, uday));
                        startActivity(new Intent(this, MainActivity.class));
                }
            }

        });

    }
    public static void createChannelIfNeeded(NotificationManager manager){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(notificationChannel);
        }
    }
}
