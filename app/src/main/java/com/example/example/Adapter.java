package com.example.example;

import static androidx.core.content.ContextCompat.startActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.example.example.data.AppDatabase;
import com.example.example.model.Notes;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class Adapter extends RecyclerView.Adapter<Adapter.NoteViewHolder> {

    private ArrayList<Notes> sortedList;

    public Adapter() {
        sortedList = new ArrayList<>();
        sortedList = (ArrayList<Notes>) MainActivity.appDatabase.noteDao().getAll();
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NoteViewHolder( parent.getContext(),
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note_list, parent, false), this );
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        holder.note = sortedList.get(position);
    }

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    public void setItems(List<Notes> notes) {
        sortedList.clear();
        sortedList.addAll(notes);
    }

     static class NoteViewHolder extends RecyclerView.ViewHolder {

         TextView noteText;
         Adapter adapter;
         View delete;
         Notes note = new Notes();


         public NoteViewHolder(Context context, @NonNull final View itemView, Adapter adapter) {
             super(itemView);
             this.adapter = adapter;

             noteText = itemView.findViewById(R.id.note_text);
             delete = itemView.findViewById(R.id.delete);

             Intent intent = new Intent(context, UserNoteActivity.class);

             noteText.setText(MainActivity.appDatabase.noteDao().getText().toString());

             itemView.setOnClickListener(view -> startActivity(context, intent, null));
             delete.setOnClickListener(view ->{
                 MainActivity.appDatabase.noteDao().deleteNoteByUid(note.uid);
                 adapter.notifyDataSetChanged();
             });

         }

     }
}