package com.example.example;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WeatherFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeatherFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button search;
    private EditText sity;
    protected TextView result_tempreture;
    protected TextView result_pressure;

    public WeatherFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WeatherFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WeatherFragment newInstance(String param1, String param2) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        search = view.findViewById(R.id.search);
        sity = view.findViewById(R.id.sity);
        result_tempreture = view.findViewById(R.id.result_tempreture);
        result_pressure = view.findViewById(R.id.result_pressure);

        search.setOnClickListener(v -> {
            if (sity.getText().toString().trim().equals("")){
                Toast.makeText(requireContext(), "Вам нужно ввести город", Toast.LENGTH_SHORT).show();
            }else {
                String user_city = sity.getText().toString();
                String key = "04789c597d3d85316d8a91cf5ef24479";
                String url = "https://api.openweathermap.org/data/2.5/weather?q=" + user_city + "&appid="+ key +"&units=metric&lang=ru";

                new GetURL().execute(url);
            }
        });

        return view;
    }

    private class GetURL extends AsyncTask<String, String, String> {

        protected void onPreExecute(){
            super.onPreExecute();
            result_tempreture.setText("Ожидайте...");
            result_pressure.setText("Ожидайте...");
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null)
                    buffer.append(line).append("\n");
                return buffer.toString();
            } catch (IOException e) {
                Toast.makeText(requireContext(), "Ошибка", Toast.LENGTH_SHORT).show();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                try {
                    if (reader != null)
                        reader.close();
                }catch (IOException e){
                    Toast.makeText(requireContext(), "Ошибка", Toast.LENGTH_SHORT).show();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            try {
                JSONObject object = new JSONObject(result);
                result_tempreture.setText(object.getJSONObject("main").getString("temp") + " °C");
                result_pressure.setText(object.getJSONObject("main").getString("pressure") + " гПа");
            } catch (JSONException e) {
                Toast.makeText(requireContext(), "Ошибка", Toast.LENGTH_SHORT).show();
            }

        }
    }
}