package com.example.example;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.example.model.Notes;

import java.util.List;

public class MainViewModel extends ViewModel {
    private LiveData<List<Notes>> noteLiveData = MainActivity.appDatabase.noteDao().getAllLiveData();

    public LiveData<List<Notes>> getNoteLiveData() {
        return noteLiveData;
    }
}

