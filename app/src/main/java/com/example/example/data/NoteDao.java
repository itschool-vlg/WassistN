package com.example.example.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.example.model.Notes;

import java.util.List;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM Notes")
    List<Notes> getAll();

    @Query("SELECT user_text FROM Notes")
    List<String> getText();

    @Query("SELECT year FROM Notes")
    List<Long> getYear();

    @Query("SELECT month FROM Notes")
    List<Long> getMonth();

    @Query("SELECT day FROM Notes")
    List<Long> getDay();

    @Query("SELECT * FROM Notes")
    LiveData<List<Notes>> getAllLiveData();


    @Query("SELECT * FROM Notes WHERE uid IN (:noteIds)")
    List<Notes> loadAllByIds(int[] noteIds);

    @Query("SELECT * FROM Notes WHERE uid = :uid LIMIT 1")
    Notes findById(int uid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Notes note);

    @Update
    void update(Notes note);

    @Query("DELETE  FROM Notes WHERE Notes.uid= :id")
    void deleteNoteByUid(int id);

    @Delete
    void delete(Notes note);

}

