package com.example.example.data;

import android.provider.ContactsContract;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.example.model.Notes;

@Database(entities = {Notes.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NoteDao noteDao();
}