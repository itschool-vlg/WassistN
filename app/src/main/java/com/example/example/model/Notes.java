package com.example.example.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Notes {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "user_text")
    public String user_text;

    @ColumnInfo(name = "year")
    public long year;

    @ColumnInfo(name = "month")
    public long month;

    @ColumnInfo(name = "day")
    public long day;

    public Notes() {
    }

    public Notes(String user_text, long year, long month, long day) {
        this.user_text = user_text;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notes notes = (Notes) o;
        return uid == notes.uid && year == notes.year && month == notes.month && day == notes.day && Objects.equals(user_text, notes.user_text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, user_text, year, month, day);
    }
}
